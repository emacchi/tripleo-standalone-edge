# EXAMPLE: 1 interface
# TODO(aschultz): This still assumes a bunch of undercloud stuff that may
# not be needed anymore. Will need to clean this up.
export IP=192.168.0.12
export NETMASK=24
# We need the gateway as we'll be reconfiguring the eth0 interface
export GATEWAY=192.168.0.1
export INTERFACE=eth0

cat <<EOF > $HOME/standalone_parameters.yaml
parameter_defaults:
  CertmongerCA: local
  CloudName: $IP
  ContainerImagePrepare:
  - set:
      ceph_image: daemon
      ceph_namespace: docker.io/ceph
      ceph_tag: v3.0.3-stable-3.0-luminous-centos-7-x86_64
      name_prefix: centos-binary-
      name_suffix: ''
      namespace: docker.io/tripleomaster
      neutron_driver: null
      tag: current-tripleo
    tag_from_label: rdo_version
  # default gateway
  ControlPlaneStaticRoutes:
    - ip_netmask: 0.0.0.0/0
      next_hop: $GATEWAY
      default: true
  Debug: true
  DeploymentUser: $USER
  DnsServers:
    - 1.1.1.1
    - 8.8.8.8
  # needed for vip & pacemaker
  KernelIpNonLocalBind: 1
  DockerInsecureRegistryAddress:
  - $IP:8787
  NeutronPublicInterface: $INTERFACE
  # domain name used by the host
  NeutronDnsDomain: localdomain
  # re-use ctlplane bridge for public net
  NeutronBridgeMappings: datacentre:br-ctlplane
  NeutronPhysicalBridge: br-ctlplane
  # enable to force metadata for public net
  #NeutronEnableForceMetadata: true
  StandaloneEnableRoutedNetworks: false
  StandaloneHomeDir: $HOME
  StandaloneLocalMtu: 1400
  # Needed if running in a VM
  StandaloneExtraConfig:
    nova::compute::libvirt::services::libvirt_virt_type: qemu
    nova::compute::libvirt::libvirt_virt_type: qemu
    oslo_messaging_notify_node_names: standalone-central.internalapi.localdomain
    oslo_messaging_rpc_node_names: standalone-central.internalapi.localdomain
    oslo_messaging_notify_password: SfUrwZateQSkPIlkU6rWjqFwG
    oslo_messaging_rpc_password: SfUrwZateQSkPIlkU6rWjqFwG
    oslo_messaging_notify_use_ssl: false
    oslo_messaging_rpc_use_ssl: false
    memcached_node_ips: ['192.168.0.13']
EOF

sudo openstack tripleo deploy \
  --templates \
  --local-ip=$IP/$NETMASK \
  -e /usr/share/openstack-tripleo-heat-templates/environments/standalone.yaml \
  -r /usr/share/openstack-tripleo-heat-templates/roles/Standalone.yaml \
  -e $HOME/standalone_parameters.yaml \
  --output-dir $HOME \
  --standalone
